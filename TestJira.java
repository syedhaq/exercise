package com.example.tests;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class TestJira {
  private WebDriver driver;
  private String baseUrl;
  private String issueSummary = "Haq issue";
  private String issueDesc = "Please contact me to know more details about my issues";  
  
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://jira.atlassian.com";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	issueSummary += "_" + getRandomValidInputAmount();
  }

  @Test
  public void testCreateIssue() throws Exception {
    createIssue();
    driver.get(baseUrl+"/issues/?jql=summary ~ '" + issueSummary + "'");
    assertEquals(issueSummary, driver.findElement(By.xpath(".//*[@id='summary-val']")).getText());
    assertEquals(issueDesc, driver.findElement(By.xpath(".//*[@id='description-val']/div/p")).getText());
  }
  
  @Test
  public void testUpdateIssue() throws Exception {
    createIssue();
	driver.findElement(By.cssSelector("span.trigger-label")).click();
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys(issueDesc+" updated");
    driver.findElement(By.id("edit-issue-submit")).click();
    
    driver.get(baseUrl+"/issues/?jql=summary ~ '" + issueSummary + "'");
    assertEquals(issueSummary, driver.findElement(By.xpath(".//*[@id='summary-val']")).getText());
    assertEquals(issueDesc+" updated", driver.findElement(By.xpath(".//*[@id='description-val']/div/p")).getText());
  }
  
  @Test
  public void testSearchIssue() throws Exception {
    createIssue();
	searchIssue();	
	
	driver.get(baseUrl+"/issues/?jql=summary ~ '" + issueSummary + "'");
    assertEquals(issueSummary, driver.findElement(By.xpath(".//*[@id='summary-val']")).getText());
    assertEquals(issueDesc, driver.findElement(By.xpath(".//*[@id='description-val']/div/p")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();    
  }

  private void createIssue() throws Exception {
    driver.get(baseUrl+"/browse/TST");
    logIn();
    waitForElement(".//*[@id='create_link']");
    //driver.findElement(By.id("create_link")).click();
    driver.findElement(By.id("create_link")).sendKeys(Keys.ENTER);
    driver.findElement(By.id("summary")).clear();
    driver.findElement(By.id("summary")).sendKeys(issueSummary);            
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys(issueDesc);
    driver.findElement(By.id("create-issue-submit")).click();    
    driver.findElement(By.partialLinkText(issueSummary)).click();
  }
  
  private void logIn() {
    driver.findElement(By.linkText("Log In")).click();
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("syed.haq@gmail.com");
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("test1234");
    driver.findElement(By.id("login-submit")).click();
  }
  
  private void searchIssue() throws Exception {
    //waitForElement(".//*[@id='quickSearchInput']");  
    //driver.findElement(By.id("quickSearchInput")).sendKeys(issueSummary+Keys.ENTER);    
    driver.get(baseUrl+"/issues/?jql=summary ~ '" + issueSummary + "'");
    //driver.findElement(By.partialLinkText(issueSummary)).click();
  }
  
  private void waitForElement (String elementxpath){
	  WebDriverWait wait = new WebDriverWait(driver, 60);
      wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementxpath)));
  }
  
  private String getRandomValidInputAmount() {
        Random generator = new Random();
        return String.valueOf(1000 + generator.nextInt(99999));
  }  
}
